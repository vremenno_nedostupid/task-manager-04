# PROJECT INFO

TASK MANAGER

# DELEVOPER INFO

**NAME**: FEDUN ALEXANDER

**E-MAIL**: sasha171998@gmail.com

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# TECHNOLOGY STACK

- Java 
- Intellej IDEA 2020.2 Ultimate
- Git

# HARDWARE

- CPU: AMD Ryzen 5 or gigher
- RAM: 8GB or more

# PROGRAM RUN 
```bash
java -jar ./task-manager-04.jar
```

# CI / CD CURRENT BUILD
https://gitlab.com/vremenno_nedostupid/task-manager-04/-/pipelines